%global _description \
dnspython is a DNS toolkit for Python. It supports \
almost all record types. It can be used for queries, \
zone transfers, and dynamic updates. It supports TSIG \
authenticated messages and EDNS0.\
dnspython provides both high and low level access to DNS. \
The high level classes perform queries for data of a given \
name, type, and class, and return an answer set. The low \
level classes allow direct manipulation of DNS zones, \
messages, names, and records.

%global sum DNS toolkit for Python

Name:           python-dns
Summary:        %{sum}
Version:        1.16.0
Release:        3
License:        MIT
URL:            http://www.dnspython.org/
Source0: 	http://www.dnspython.org/kits/%{version}/dnspython-%{version}.tar.gz

BuildArch:      noarch
Patch0:         unicode_label_escapify.patch
Patch1:         collections_abc.patch
Patch2:         base64.patch
Patch3:         fix-failed-tests.patch 

BuildRequires:  python3-devel python3-setuptools python3-pycryptodome python3-ecdsa

%description
%{_description}

%package     -n python3-dns
Summary:        %{sum}
%{?python_provide:%python_provide python3-dns}
Requires:       python3-pycryptodome python3-ecdsa

%description -n python3-dns
%{_description}

%package_help

%prep
%autosetup -p1 -n dnspython-%{version}
find examples -type f | xargs chmod a-x

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-dns
%doc LICENSE
%{python3_sitelib}/*egg-info
%{python3_sitelib}/dns

%files help
%doc examples

%changelog
* Tue Nov 17 2020 jinzhimin<jinzhimin2@huawei.com> - 1.16.0-3
- fix tests failed

* Tue Nov 10 2020 jinzhimin<jinzhimin2@huawei.com> - 1.16.0-2
- fix the network tests failed

* Tue Nov 10 2020 jinzhimin<jinzhimin2@huawei.com> - 1.16.0-1
- update to 1.16.0

* Wed Oct 21 2020 jinzhimin<jinzhimin2@huawei.com> - 1.15.0-11
- remove python2-dns subpackage

* Fri Dec 6 2019 caomeng<caomeng5@huawei.com> - 1.15.0-10
- Package init
